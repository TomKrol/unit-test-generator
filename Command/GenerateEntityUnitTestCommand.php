<?php

namespace UnitTests\Generator\EntityBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnitTests\Generator\EntityBundle\Model\Property;
use UnitTests\Generator\EntityBundle\Model\PropertyCollection;
use UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes\ClassPrototypes;
use UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes\FilePrototypes;
use UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes\MethodPrototypes;
use UnitTests\Generator\EntityBundle\Dictionary\PathChoice;

/**
 * Class GenerateEntityUnitTestCommand
 * @package UnitTests\Generator\EntityBundle\Command
 */
class GenerateEntityUnitTestCommand extends Command
{
  
    /**
    * @var SymfonyStyle
    */
    private $io;
    /**
     * @var array
     */
    private $getterSignatures = ['get', 'is', 'has'];
    /**
     * @var array
     */
    private $setterSignatures = ['set'];

    /**
     *
     */
    protected function configure() : void
    {
        $this->setName('unit-tests:generate:entity')
            ->setDescription('Generates simple unit test structure for a given entity')
            ->addArgument('entityNamespace', InputArgument::REQUIRED, 'fqn for target entity');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        $this->io = new SymfonyStyle($input,$output);
        $targetEntityNamespace = $input->getArgument('entityNamespace');
        $targetEntityReflection = new \ReflectionClass($targetEntityNamespace);

        $methodList = $targetEntityReflection->getMethods();

        $skippedCounter = 0;
        $matchedCounter = 0;
        $propertyCollection = new PropertyCollection();
        $unmatchedProperties = [];
        foreach($targetEntityReflection->getProperties() as $reflectionProperty) {
            $getter = $this->findMethod($reflectionProperty->getName(), $this->getterSignatures, $methodList);
            $setter = $this->findMethod($reflectionProperty->getName(), $this->setterSignatures, $methodList);

            if(null === $getter || null === $setter) {
                ++$skippedCounter;
                $unmatchedProperties[] = $reflectionProperty;
                $output->writeln('<comment>Property [' . $reflectionProperty->getName() . '] does not seem to have either standard getter or setter method.</comment>');
            } else {
                ++$matchedCounter;
                $property = new Property($reflectionProperty, $getter, $setter);
                $propertyCollection->add($property);
            }
        }

        $bundleNamespace = $this->bundleNamespace($targetEntityReflection->getNamespaceName());
        
        $chooseDirectory = $this->io->choice('Where create unit-test?', PathChoice::GROUP, PathChoice::PATH_GLOBAL);
             
        switch($chooseDirectory){
          case PathChoice::PATH_GLOBAL:
            $this->io->success('Your choice is Global in project.');
            $bundleDirectory = "tests/".$bundleNamespace;
            break;
          case PathChoice::PATH_LOCAL:
            $this->io->success('Your choice is local in bundle.');
            $bundleDirectory = $this->bundleDirectory($targetEntityReflection->getFileName());
          break;
          case PathChoice::YOUR_PATH:
            $this->io->success('Your choice is your path...');
            $path = $this->io->ask('Please, write your path: ','tests/');
            $bundleDirectory = "./".$path.$bundleNamespace;
          break;
        }
        
        $testDirectory = $bundleDirectory . '/Tests/Unit/';
        $testClass = $targetEntityReflection->getShortName() . 'Test';
        $testFilename = $testClass . '.php';

        if(!file_exists($testDirectory)) {
            throw new \RuntimeException(sprintf('Test directory [%s] does not exist.', $testDirectory));
        }

        $testNamespace = $bundleNamespace . '\\Tests\\Unit';

        $filePrototypes = new FilePrototypes();
        $classPrototypes = new ClassPrototypes();
        $methodPrototypes = new MethodPrototypes();

        $file = new \SplFileObject($testDirectory . $testFilename, 'w');
        $file->fwrite($filePrototypes->createOpeningTag());
        $file->fwrite($classPrototypes->createNamespace($testNamespace));
        $file->fwrite($classPrototypes->createUse('PHPUnit\\Framework\\TestCase'));
        $file->fwrite($classPrototypes->createClassSignature($testClass));

        foreach($propertyCollection as $property) {
            $file->fwrite($methodPrototypes->createMethodSignature($property->property()->getName()));
            $file->fwrite($methodPrototypes->createMethodBody(
                $targetEntityNamespace,
                $property->setter(),
                $property->getter(),
                $property->setterParameterType()
            ));
            $file->fwrite($methodPrototypes->createMethodClosingTag());
        }

        foreach($unmatchedProperties as $property) {
            $file->fwrite($methodPrototypes->createMethodSignature($property->getName()));
            $file->fwrite($methodPrototypes->createMethodStub());
            $file->fwrite($methodPrototypes->createMethodClosingTag());
        }

        $file->fwrite($classPrototypes->createClosingTag());
        $file->fwrite($filePrototypes->createClosingTag());

        $output->writeln('');
        $output->writeln('Summary:');
        $output->writeln('Matched properties: ' . $matchedCounter);
        $output->writeln('Skipped properties: ' . $skippedCounter);
        $output->writeln('');
    }

    /**
     * @param string $propertyName
     * @param [] $methodSignatures
     * @param \ReflectionMethod[] $methods
     * @return null|\ReflectionMethod
     */
    private function findMethod($propertyName, $methodSignatures, $methods) 
    {
        foreach($methodSignatures as $methodSignature) {
            $methodName = $methodSignature . ucfirst($propertyName);

            foreach($methods as $method) {
                if(preg_match('/^' . $methodName . '$/', $method->getName())) {
                    return $method;
                }
            }
        }
        // @TODO: zastanowić się nad null object
        return null;
    }

    /**
     * @param $namespace
     * @return string
     */
    private function bundleNamespace($namespace) : string
    {
        return $this->bundleLocation($namespace, '\\');
    }

    /**
     * @param $path
     * @return string
     */
    private function bundleDirectory($path) : string
    {
        return $this->bundleLocation($path, DIRECTORY_SEPARATOR);
    }

    /**
     * @param $path
     * @param $separator
     * @return string
     */
    private function bundleLocation($path, $separator) : string
    {
        $pathParts = explode($separator, $path);
        $pathPartsRev = array_reverse($pathParts);
        $bundlePathParts = [];

        $found = false;
        foreach($pathPartsRev as $pathPart) {
            if(preg_match('/Bundle$/', $pathPart)) {
                $found = true;
            }

            if($found) {
                $bundlePathParts[] = $pathPart;
            }
        }

        return join($separator, array_reverse($bundlePathParts));
    }
}