<?php

namespace UnitTests\Generator\EntityBundle\Dictionary;

class PathChoice{
  
  const PATH_GLOBAL = "Global in project";
  
  const PATH_LOCAL = "Local in bundle";
  
  const YOUR_PATH = "Your path to phpunit test";
  
  const GROUP = [
    PathChoice::PATH_GLOBAL,
    PathChoice::PATH_LOCAL,
    PathChoice::YOUR_PATH,
  ];
  
}