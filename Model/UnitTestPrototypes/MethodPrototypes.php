<?php

namespace UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes;


/**
 * Class MethodPrototypes
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes
 */
class MethodPrototypes
{
    /**
     * @param $methodName
     * @return string
     */
    public function createMethodSignature($methodName) : string
    {
        $string = sprintf('    public function test%s()', ucfirst($methodName)) . PHP_EOL;
        $string .= sprintf('    {') . PHP_EOL;

        return $string;
    }

    /**
     * @return string
     */
    public function createMethodClosingTag() : string
    {
        $string = sprintf('    }') . PHP_EOL;

        return $string;
    }

    /**
     * @param $object
     * @param \ReflectionMethod $setter
     * @param \ReflectionMethod $getter
     * @param $testValue
     * @return string
     */
    public function createMethodBody($object, $setter, $getter, $testValue) : string
    {
        $string = sprintf('        $object = new \%s();', $object) . PHP_EOL;
        $string .= sprintf('        $this->assertInstanceOf(\%s::class, $object->%s(%s));', $object, $setter->getShortName(), $testValue) . PHP_EOL;
        $string .= sprintf('        $this->assertEquals(%s, $object->%s());', $testValue, $getter->getShortName()) . PHP_EOL;

        return $string;
    }

    /**
     * @return string
     */
    public function createMethodStub() : string
    {
        $string = sprintf('        // @TODO: method stub') . PHP_EOL;

        return $string;
    }
}