<?php

namespace UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes;


/**
 * Class FilePrototypes
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes
 */
class FilePrototypes
{
    /**
     * @return string
     */
    public function createOpeningTag() : string
    {
        $string = sprintf('<?php') . PHP_EOL;

        return $string;
    }

    /**
     * @return string
     */
    public function createClosingTag() : string
    {
        $string = sprintf('');

        return $string;
    }
}