<?php

namespace UnitTests\Generator\EntityBundle\Model;


/**
 * Class Property
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model
 */
/**
 * Class Property
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model
 */
/**
 * Class Property
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model
 */
class Property
{
    /** @var \ReflectionProperty */
    private $property;

    /** @var \ReflectionMethod */
    private $getter;

    /** @var \ReflectionMethod */
    private $setter;

    /**
     * Property constructor.
     * @param $property
     * @param $getter
     * @param $setter
     */
    public function __construct($property, $getter, $setter)
    {
        $this->property = $property;
        $this->getter = $getter;
        $this->setter = $setter;
    }

    /**
     * @return \ReflectionProperty
     */
    public function property() : \ReflectionProperty
    {
        return $this->property;
    }

    /**
     * @return \ReflectionMethod
     */
    public function getter() : \ReflectionMethod
    {
        return $this->getter;
    }

    /**
     * @return \ReflectionMethod
     */
    public function setter() : \ReflectionMethod
    {
        return $this->setter;
    }

    /**
     * @return string
     */
    public function setterParameterType() : string
    {
        $parameters = $this->setter->getParameters();

        $parameterType = $parameters[0]->getClass();

        if ($parameterType === null) {
            return '"TEST"';
        }

        switch ($parameters[0]->getClass()->getName()) {
            case 'DateTime':
                $dateTime = new \DateTime();
                return sprintf('new \\%s("%s")', $parameters[0]->getClass()->getName(), $dateTime->format('Y-m-d'));
            case 'Money\\Money':
                return sprintf('new \\%s(123, new \\Money\\Currency("PLN"))', $parameters[0]->getClass()->getName());
            default:
                return sprintf('new \\%s()', $parameters[0]->getClass()->getName());
        }
    }
}