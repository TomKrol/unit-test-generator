# UnitTests Generator #

## Entity ##

Bundle is used to generate simple unit tests of Entities.

The only thing you need to pass is the Entity namespace:


### Symfony 3.3 ### 
```bash
$ php bin/console unit-tests:generate:entity "<entity_namespace>"
```
## Used ##

```bash
$ php bin/console unit-tests:generate:entity "AppBundle\Entity\MyEntity"
```

It will create simple unit test with Entity name in _Bundle/Tests/_ directory or tests/ or your path in project.

!!! Just remember to escape backslashes !!!

## Installation ##

### For Symfony Framework >= 3.3 ###

Require the bundle and its dependencies with composer:

```bash
$ composer require tomasz-kr/unittest-generator-entity
```

Register the bundle:

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new KCH\Bundle\UnitTests\Generator\EntityBundle\UnitTestsGeneratorEntityBundle(),
    );
}
```

## Contributors ##
[Tomasz Król](http://tomaszkrol.eu)

## License ##
MIT

## Fork ##
[Oryginal repository](https://bitbucket.org/wjts/unit-test-generator.git)